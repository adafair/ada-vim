"
" My Plugins - START
"
call plug#begin('~/.local/share/nvim/plugins')

Plug 'itchyny/lightline.vim'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fugitive'
Plug 'ntpeters/vim-better-whitespace'
Plug 'mileszs/ack.vim'
Plug 'tpope/vim-commentary'

" Fuzzy Finders
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'

" Writing
Plug 'junegunn/goyo.vim'
Plug 'reedes/vim-pencil'
Plug 'plasticboy/vim-markdown'

" Ruby
Plug 'vim-ruby/vim-ruby', { 'for': 'ruby' }

" Javascript
Plug 'pangloss/vim-javascript'

" Typescript
" Plug 'leafgarland/typescript-vim'

" Themes
Plug 'rakr/vim-two-firewatch'
Plug 'archseer/colibri.vim'
Plug 'nightsense/strawberry'
Plug 'yuttie/hydrangea-vim'
Plug 'rakr/vim-one'
Plug 'atelierbram/Base2Tone-vim'
Plug 'arcticicestudio/nord-vim'
Plug 'noahfrederick/vim-hemisu'
Plug 'kamwitsta/flatwhite-vim'
Plug 'dracula/vim'

call plug#end()
"
" My Plugins - END
"

" Required:
filetype plugin indent on
syntax enable

"
" Configuration Options - START
"

" Color
set termguicolors

" Enable mouse support
set mouse=a

" Enable clipboard integration with OS
set clipboard+=unnamedplus

" Line numbers
set number
set relativenumber

" Search highlighting
set hlsearch

" Lightline
set noshowmode
let g:lightline = {
  \ 'colorscheme': 'twofirewatch'
  \ }

" Vim Better Whitespace
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1

" Writing
let g:pencil#wrapModeDefault = 'soft'
let g:vim_markdown_folding_disabled = 1

"
" Configuration Options - END
"

"
" Commands and Keymappings - START
"

let mapleader = ','

command! Config :edit ~/.config/nvim/init.vim
command! Zen    :Goyo | :PencilToggle

" Tabs
command! IndentSpaces
  \ set tabstop=2    |
  \ set expandtab    |
  \ set shiftwidth=2 |
  \ set autoindent

command! IndentTabs
  \ set tabstop=4    |
  \ set noexpandtab  |
  \ set shiftwidth=0 |
  \ set autoindent

nnoremap <Leader>f :Files<CR>
nnoremap <Leader>b :Buffers<CR>
nnoremap <Leader>g :GFiles<CR>
nnoremap <Leader>t :NERDTreeToggle<CR>
nnoremap <Leader>r :NERDTreeFind<CR>
nnoremap <Leader>fq :bd<CR>
nnoremap <Leader>w <C-w>
nnoremap <Leader>q :close<CR>
nnoremap <Leader>s :vsplit<CR>
nnoremap <Leader>d :split<CR>
nnoremap <Leader>n :vnew<CR>

" Colorschemes
command! Light :colorscheme two-firewatch | :set background=light
command! Dark :colorscheme two-firewatch | :set background=dark

"
" Commands and Keymappings - END
"

"
" Final Setup
"

" Default to using spaces for indentation
IndentSpaces

" Default Colorscheme
Dark
